'use strict';

/**
 * @ngdoc function
 * @name angularCrudApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularCrudApp
 */
angular.module('angularCrudApp')
    .controller('MainCtrl', function ($scope) {
        $scope.EmpList = [{
            id: '1',
            name: 'Jaydeep',
            empDate: '05/01/2017',
            age: '23',
            gender: 'Male'
        }, {
            id: '2',
            name: 'Akshay',
            empDate: '01/12/2016',
            age: '25',
            gender: 'Male'
        }, {
            id: '3',
            name: 'Mayur',
            empDate: '01/11/2015',
            age: '35',
            gender: 'Male'
        }];

        $scope.getUUID = function () {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }

            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        }

        $scope.submitEmp = function (empModel) {
            if (empModel.hasOwnProperty('id')) {
                var index;
                $scope.EmpList.map(function (item, ind) {
                    if (item.id === empModel.id) {
                        index = ind;
                    }
                });
                $scope.EmpList[index] = empModel;
            } else {
                empModel.id = $scope.getUUID();
                $scope.EmpList.push(empModel);
            }
            clearModel();
        };

        $scope.delete = function (Emp) {
            var index;
            $scope.EmpList.map(function (item, ind) {
                if (item.id === Emp.id) {
                    index = ind;
                }
            });
            $scope.EmpList.splice(index, 1);
        };

        $scope.bindSelectedData = function (emp) {
            $scope.empModel = angular.copy(emp);
        };

        function clearModel() {
            $scope.empModel = {};
        }
    });
