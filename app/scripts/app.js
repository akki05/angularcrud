'use strict';

/**
 * @ngdoc overview
 * @name angularCrudApp
 * @description
 * # angularCrudApp
 *
 * Main module of the application.
 */
angular
  .module('angularCrudApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
      '720kb.datepicker',
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
       /* controllerAs: 'main'*/
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
       /* controllerAs: 'about'*/
      })
      .otherwise({
        redirectTo: '/'
      });
  });
