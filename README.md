# angular-crud

## Build & development

Run  `npm install` and `bower install`
Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
